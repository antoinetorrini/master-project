#!/usr/bin/env python
# coding: utf-8

# # Master Project
# 
# This notebook contains the totality of the code used for my Master Project.

# ## Table of contents
# 
# - [Data Collection](#part1)
# - [Data Cleaning](#part2)
#     - [Cleaning and removing non english tweets](#part2sec1)
#     - [Remove stopwords](#part2sec2)
#     - [Lemmatizing](#part2sec3)
#     - [Stemming](#part2sec4)
# - [Exploratory Analysis](#part3)
#     - [Most frequent words](#part3sec1)
#     - [Trends](#part3sec2)
#     - [Wordclouds](#part3sec3)
#     - [Bigrams and Trigrams](#part3sec4)
# - [Sentiment Analysis](#part4)
# - [Clustering](#part5)
#     - [KMeans](#part5sec1)
#     - [LDA](#part5sec2)
# - [Twitter Network](#part6)
# - [Emoji?](#part7)
# - [CLI Helper](#helper)

# # Data collection<a id='part1'></a>
# 
# I used the `twitterscraper` package with the following query.
# 
# ```
# twitterscraper volkswagen -bd 2015-09-01 -ed 2015-10-15 -o 01_09_2015-15_10_2015.json
# ```

# In[1]:


import re
import json
import random

import joblib

import numpy as np
import pandas as pd

from PIL import Image

from tqdm import tnrange

from langdetect import detect
from langdetect.lang_detect_exception import LangDetectException

from wordcloud import WordCloud

import matplotlib.pyplot as plt

from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

from nltk.corpus import stopwords
from nltk.probability import FreqDist
from nltk.tokenize import regexp_tokenize
from nltk.stem import SnowballStemmer, WordNetLemmatizer
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk.collocations import BigramAssocMeasures, BigramCollocationFinder
from nltk.collocations import TrigramAssocMeasures, TrigramCollocationFinder


# # Data cleaning<a id='part2'></a>
# 
# Let's open the raw data.

# In[2]:

try:
    labels = ['fullname', 'html', 'id', 'likes', 'replies', 'retweets', 'text', 'timestamp', 'url', 'user']
    arr = np.load('Data/data.npy', allow_pickle=True)
    df_full = pd.DataFrame(arr, columns=labels)
except FileNotFoundError:
    with open('Data/01_09_2015-15_10_2015.json', 'r') as file:
        raw = json.load(file)
    df_full = pd.DataFrame(raw)
    np.save('Data/data.npy', df_full.values)


# We have collected **914 274** tweets.

# In[3]:


print(f'Number of tweets collected: {len(df_full)}')


# A tweet looks like this.

# In[4]:


df_full.iloc[1,]


# To conduct our analysis, we will only keep the `id` and the `text` columns. Later on, we might rely on likes, replies and retweets to give importance to tweets.

# In[5]:


df = df_full.drop(columns=['fullname', 'html', 'likes', 'replies', 'retweets', 'timestamp', 'url', 'user'])
df.head()


# ## Cleaning and removing non english tweets<a id='part2sec1'></a>
# 
# We removed the links.

# In[6]:


print('Original:')
print(df['text'][1])
print(df['text'][18])
print('Transformed:')
print(re.sub(r'(https?:\/\/|pic.twitter)[^\s]+', '', df['text'][1]))
print(re.sub(r'(https?:\/\/|pic.twitter)[^\s]+', '', df['text'][18]))


# We remove the user tags or hastags but not the text associated with it.

# In[7]:


print('Original:')
print(df['text'][0])
print('Transformed:')
print(re.sub(r'(#|@[^\s]\w+)', '', df['text'][0]))


# Let's write a function that sums up all the cleaning.
# 
# We also write a function to keep only the english tweets.

# In[8]:


def cleaner(df):
    # Create a copy
    df_tmp = df.copy()
    
    # Links cleaning
    df_tmp['text'] = df_tmp['text'].str.replace(r'(https?:\/\/|pic.twitter)[^\s]+', '')
    
    # @ and # cleaning
    df_tmp['text'] = df_tmp['text'].str.replace(r'(#|@[^\s]\w+)', '')
    
    # Remove the numbers
    df_tmp['text'] = df_tmp['text'].str.replace(r'\d+', '')
    
    # Put everything to lower case
    df_tmp['text'] = df_tmp['text'].str.lower()
    
    # Punctuation
    df_tmp['text'] = df_tmp['text'].str.replace(r'[^\w\s]', ' ')
    
    # Strip whitespaces
    df_tmp['text'] = df_tmp['text'].str.replace(r'\s{2,}', ' ')
    
    return df_tmp

def english_keeper(df):    
    english_tweets = []
    
    for i in tnrange(len(df)):
        text = df['text'].iloc[i]
        try:
            if detect(text) == 'en':
                english_tweets.append((df['id'].iloc[i], text))
        except LangDetectException:
            pass
    
    df_english = pd.DataFrame.from_records(english_tweets, columns=['id', 'text'])
    
    return df_english


# In[9]:


splits = np.split(df, 27)
clean_splits = []
labels = ['id', 'text']

for i in tnrange(len(splits)):
    try:
        arr = np.load(f'Processing/split_{i}.npy', allow_pickle=True)
        clean_splits.append(pd.DataFrame(arr, columns=['id', 'text']))
        print(f'Loaded from index {splits[i].index._start} to {splits[i].index._stop}')
    except FileNotFoundError:
        print(f'Cleaning from index {splits[i].index._start} to {splits[i].index._stop}')
        df_clean = cleaner(splits[i])
        df_english = english_keeper(df_clean)
        clean_splits.append(df_english)
        np.save(f'Processing/split_{i}.npy', df_english.values)


# With this cleaning, we loose **62%** of the database, resulting in **339 367** tweets remaining. It does insure a better quality of tweets and will reduce computation time.

# In[10]:


df_clean = pd.concat(clean_splits)
print(df_clean.shape)


# In[11]:


df_clean['text'].head()


# ## Remove stopwords<a id='part2sec2'></a>

# In[12]:


english_stopwords = stopwords.words('english')

print('Original:')
print(df_clean.iloc[4, 1])
print('Transformed:')
print(' '.join([w for w in df_clean.iloc[4, 1].split(' ') if w not in english_stopwords]).strip())


# In[13]:


def remove_stopwords(df, add=None):
    english_stopwords = stopwords.words('english')
    
    if type(add) == list:
        for e in add:
            english_stopwords.append(e)
            
    words_tweets = []
    
    for i in tnrange(len(df)):
        text = ' '.join([w for w in df['text'].iloc[i].split(' ') if w not in english_stopwords]).strip()
        words_tweets.append((df['id'].iloc[i], text))

    df_words = pd.DataFrame.from_records(words_tweets, columns=['id', 'text'])
    
    return df_words


# In[14]:


df_words = remove_stopwords(df_clean, add=['volkswagen', 'vw'])


# ## Lemmatizing <a id='part2sec3'></a>

# In[15]:


wnl = WordNetLemmatizer()

print('Original:')
print(df_words.iloc[1300, 1])
print('Transformed:')
print(' '.join([wnl.lemmatize(w, pos='v') for w in df_words.iloc[1300, 1].split(' ')]).strip())


# In[16]:


def lemmatizing(df):
    wnl = WordNetLemmatizer()
    
    lemmed_tweets = []
    
    for i in tnrange(len(df)):
        text = ' '.join([wnl.lemmatize(w, pos='v') for w in df['text'].iloc[i].split(' ')]).strip()
        lemmed_tweets.append((df['id'].iloc[i], text))

    df_lemm = pd.DataFrame.from_records(lemmed_tweets, columns=['id', 'text'])
    
    return df_lemm


# In[17]:


df_lemm = lemmatizing(df_words)


# ## Stemming<a id='part2sec4'></a>

# In[18]:


sno = SnowballStemmer('english')

print('Original:')
print(df_words.iloc[13, 1])
print('Transformed:')
print(' '.join([sno.stem(w) for w in df_lemm.iloc[13, 1].split(' ')]).strip())


# In[19]:


def stemming(df):
    sno = SnowballStemmer('english')
    
    stemmed_tweets = []
    
    for i in tnrange(len(df)):
        text = ' '.join([sno.stem(w) for w in df['text'].iloc[i].split(' ')]).strip()
        stemmed_tweets.append((df['id'].iloc[i], text))

    df_stem = pd.DataFrame.from_records(stemmed_tweets, columns=['id', 'text'])
    
    return df_stem


# In[20]:


df_stem = stemming(df_lemm)


# ## Exploratory Analysis<a id='part3'></a>
# 
# We will now dive a little into the tweets that we have gathered to see the general trend of the tweets.
# 
# > **Note**: We will use the english tweets only
# 
# Let's repopulate the `df_stem` dataset.

# In[21]:


df = pd.merge(df_stem, df_full, how='left', on='id', suffixes=('_work', '_raw'))
df['timestamp'] = pd.to_datetime(df['timestamp'])

print(df.shape)
print(df.iloc[0,])


# In[25]:


df.query('user=="volkswagen"')['text_raw']


# ## Most frequent words<a id='part3sec1'></a>
# 
# Let's look at the distribution of the most frequent words.

# In[ ]:


string_data_frequent = ' '.join([e for e in df['text_work']])
tokens_frequent = regexp_tokenize(string_data_frequent, r'\w+')
freq_dist_frequent = FreqDist(tokens_frequent)


# In[ ]:


def print_top_ten(freqdist, simple=False):
    top_ten = re.findall(r'\'(\w+)\':\s(\d+)', freqdist.pformat())
    
    if simple:
        for i in range(len(top_ten)):
            print(f'{top_ten[i][0]} ', end='')
    else:
        print('Top 10 words by frequency:')
        for i in range(len(top_ten)):
            print(f'n°{i+1} - {top_ten[i][0]} ({top_ten[i][1]})  ', end='')


# In[ ]:


print_top_ten(freq_dist_frequent)


# ## Trends<a id='part3sec2'></a>

# We will use the Tableau Desktop application to better vizualise the trends.

# In[ ]:


#df.to_csv('tableau.csv', index=False)


# ### General
# 
# We can see on the follwing graph the number of teets emitted on each day of the period studied.
# <img src='Tableau/Number of tweets.png'>
# 
# ### Map of Tweets
# 
# We can see on this map the most relevant tweets in terms of likes, replies and retweets and their contents for some of them.
# <img src='Tableau/Replies  Retweets Tweet Map.png'>
# 
# 
# ### Hours of the day
# 
# We have the number of tweets versus the Likes by Hour of the day.
# <img src='Tableau/Nb Tweets  Likes by Hours.png'>
# 
# We have the number of tweets versus the Replies by Hour of the day.
# <img src='Tableau/Nb Tweets  Replies by Hours.png'>
# 
# We have the number of tweets versus the Retweets by Hour of the day.
# <img src='Tableau/Nb Tweets  Retweets by Hours.png'>

# ## Wordclouds<a id='part3sec3'></a>
# 
# We are about to plot wordclouds. The first one is a simple wordcloud made with the totality of the cleaned text.

# In[ ]:


# Mask that we use for the wordclouds
mask = np.array(Image.open('Wordclouds/utils/mask_rond.png'))

# Global variables that can be changed if needed
MAX_WORDS = 150
MIN_FONT_SIZE = 6
BACKGROUND_COLOR = '#ffffff'
FONT_PATH = '../../Library/Fonts/Calibri.tff'

# Blue color function
def blue_color_func(word, font_size, position, orientation, random_state=None, **kwargs):
    # Fonction de couleur PIL
    return "hsl(240, 100%%, %d%%)" % random.randint(30, 70)

string_data_work = ' '.join([e for e in df['text_work']])
tokens = regexp_tokenize(string_data_work, r'\w+')

print(f'Number of tokens : {len(tokens):,}')


# To display wordclouds we have to work on tokens which are actually just a list of words. It means we have 2 846 325 words.

# In[ ]:


tokens[:10]


# Given the number of tokens, that is why we will use the frequencies to plot the most common words. It will create a dictionnary made of all words and the numbers of time they appear.

# In[ ]:


freq_dist_work = FreqDist(tokens)
print(f'Number of different words: {len(freq_dist_work):,}')
freq_dist_work


# In[ ]:


def wordcloud_generator(freq, name, mask,
                        figsize=(20, 10), 
                        max_words=MAX_WORDS,
                        min_font_size=MIN_FONT_SIZE,
                        background_color=BACKGROUND_COLOR):
    
    wc = WordCloud(
        max_words=MAX_WORDS,
        min_font_size=MIN_FONT_SIZE,
        mask=mask,
        prefer_horizontal=1,
        background_color=BACKGROUND_COLOR
    ).generate_from_frequencies(freq)
    
    plt.figure(figsize=figsize)

    plt.imshow(wc.recolor(color_func=blue_color_func, random_state=3), interpolation="bilinear")
    wc.to_file(f'Wordclouds/{name}')
    plt.axis("off");
    
    return wc


# Now let's plot the Wordcloud.

# In[ ]:


wc_work = wordcloud_generator(freq_dist_work, 'wordcloud_work.png', mask)


# It is useful to monitor the hashtags use by Twitter users.

# In[ ]:


print('Original:')
print(df['text_raw'].iloc[0])
print('Transformed:')
print(' '.join(re.findall(r'#[^\s]\w+', df['text_raw'].iloc[0])))


# In[ ]:


df['hashtags'] = df['text_raw'].str.findall(r'#[^\s]\w+').apply(lambda x: ' '.join(x))


# Once all hastags has been identified, we do the same steps as before. We create a long string of all hashtags in lowercase, we tokenize with a simple split. It results in **465 140** hashtags in the dataset.

# In[ ]:


string_data_hashtags = ' '.join([e for e in df['hashtags']]).lower()
tokens_hashtags = string_data_hashtags.split(' ')

print(f'Number of tokens : {len(tokens_hashtags):,}')


# In[ ]:


freq_dist_hashtags = FreqDist(tokens_hashtags)
print(f'Number of different words: {len(freq_dist_hashtags):,}')
freq_dist_hashtags


# We have **23 498** different hashtags used and we can now see which one are the most commons. Of course #volkswgen is the most used.

# In[ ]:


wc_hashtags = wordcloud_generator(freq_dist_hashtags, 'wordcloud_hashtags.png', mask)


# Let's do the same for mentions.

# In[ ]:


df['mentions'] = df['text_raw'].str.findall(r'@[^\s]\w+').apply(lambda x: ' '.join(x))

string_data_mentions = ' '.join([e for e in df['mentions']]).lower()
tokens_mentions = string_data_mentions.split(' ')

print(f'Number of tokens : {len(tokens_mentions):,}')

freq_dist_mentions = FreqDist(tokens_mentions)
print(f'Number of different words: {len(freq_dist_mentions):,}')
freq_dist_mentions.pprint()

wc_mentions = wordcloud_generator(freq_dist_mentions, 'wordcloud_mentions.png', mask)


# It would be interesting to observe the evolution of the most frequent terms used during the time period we study. We will split our dataset into 7.

# In[ ]:


labels = ['text_work', 'timestamp']
splitted = df.filter(labels).sort_values(by=['timestamp'])
splits = np.split(splitted, 7)


for i in range(len(splits)):
    tmp = pd.DataFrame(splits[i], columns=labels)
    string_data_tmp = ' '.join([e for e in tmp['text_work']])
    tokens_tmp = regexp_tokenize(string_data_tmp, r'\w+')
    freq_dist_tmp = FreqDist(tokens_tmp)
    wc_tmp = wordcloud_generator(freq_dist_tmp, f'wordcloud_split_{i+1}.png', mask)


# In[ ]:


df['url'].iloc[217724]


# In[ ]:


df[df['hashtags'].str.contains(r'freedomforkesha', flags=re.IGNORECASE)]['user'].astype('category').describe()


# ## Bigrams and Trigrams<a id='part3sec4'></a>

# In[ ]:


def n_grams_finder(series, nb_bigrams=10, n=2):
    if n not in [2, 3]:
        raise ValueError('The function currently supports only bigrams and trigrams. Please enter n equals to 2 or 3.')
    
    string_data = ' '.join([e for e in series])
    
    tokens = string_data.split(' ')
    
    if n == 2:
        finder = BigramCollocationFinder.from_words(tokens)
        finder.apply_freq_filter(10)
        bigram_measures = BigramAssocMeasures()
        collocations = finder.nbest(bigram_measures.pmi, nb_bigrams)
        print(collocations)
    elif n == 3:
        finder = TrigramCollocationFinder.from_words(tokens)
        finder.apply_freq_filter(10)
        trigram_measures = TrigramAssocMeasures()
        collocations = finder.nbest(trigram_measures.pmi, nb_bigrams)
        print(collocations)


# In[ ]:


n_grams_finder(df['text_work'])


# In[ ]:


n_grams_finder(df['text_work'], n=3)


# # Sentiment Analysis<a id='part4'></a>

# In[ ]:


sid = SentimentIntensityAnalyzer()
print(df['text_work'].iloc[0])
print(sid.polarity_scores(df['text_work'].iloc[0]))


# In[ ]:


df['compound'] = df['text_raw'].apply(lambda x: sid.polarity_scores(x)['compound'])


# In[ ]:


df.head()


# In[ ]:


df.to_csv('tableau.csv', index=False)


# # Clustering<a id='part5'></a>
# 
# I arbitrarily choose 6 for the max clusters to detect.

# In[27]:


N_CLUSTERS = 4


# ## KMeans<a id='part5sec1'></a>

# $$
# \begin{pmatrix}
# 1 & 1 & 1 & 0 \\
# 1 & 1 & 0 & 1
# \end{pmatrix}
# $$

# We transform a list of sentences into a Term Frequency Inverse Document Matrix.

# In[28]:


list_sentences = [e for e in df['text_work']]

vect = CountVectorizer()
list_vect = vect.fit_transform(list_sentences)

tfidf = TfidfTransformer()
list_tfidf = tfidf.fit_transform(list_vect)


# The number of cluser is set arbitrarily. 

# In[29]:


try:
    kmeans = joblib.load('Models/kmeans_4_300.joblib')
except FileNotFoundError:
    kmeans = KMeans(n_clusters=N_CLUSTERS, init='k-means++', max_iter=300, n_jobs=-1)
    kmeans.fit(list_tfidf)
    joblib.dump(kmeans, 'Models/kmeans_4_300.joblib')


# Once the models is done computing we can see the Top 10 words associated with each cluster

# In[32]:


centers_kmeans = kmeans.cluster_centers_.argsort()[:, ::-1]
terms = vect.get_feature_names()

for i in range(N_CLUSTERS):
    print(f'Cluster {i+1}:')
    for ind in centers_kmeans[i, :10]:
        print(f'{terms[ind]} ', end='')
    print('\n')


# ## LDA<a id='part5sec2'></a>
# 
# We are going to use the `gensim` package for a better implementation of the LDA Model.

# In[33]:


from gensim import corpora, models


# In[35]:


processed_docs = df['text_work'].apply(lambda x: x.split(' '))

dictionary = corpora.Dictionary(processed_docs)


# We are going to filter out the tokens that:
# - appears in less than 15 tweets (absolute number) or
# - in more than 50% of the tweets (fraction of total corpus size, not absolute number)
# - after the above two steps, keep only the first 100 000 most frequent tokens
# 
# We can see that we only keep **7 199** words.

# In[36]:


dictionary.filter_extremes(no_below=15, no_above=0.5, keep_n=100000)
print(f'Keeping only {len(dictionary):,} words.')


# For each tweet we create a dictionary reporting how many words and how many times those words appear.

# In[38]:


corpus = [dictionary.doc2bow(doc) for doc in processed_docs]


# **Using Bags of Words**

# In[39]:


try:
    lda_model = joblib.load('Models/lda_4.joblib')
except FileNotFoundError:
    lda_model = models.LdaMulticore(corpus,
                                    num_topics=N_CLUSTERS,
                                    id2word=dictionary,
                                    passes=2,
                                    workers=3)
    joblib.dump(lda_model, 'Models/lda_4.joblib')


# In[40]:


for idx, topic in lda_model.print_topics(-1):
    print(f'Topic: {idx}')
    print(f'Words: {topic}')


# **Using TF-IDF**

# In[41]:


tfidf = models.TfidfModel(corpus)
corpus_tfidf = tfidf[corpus]

try:
    lda_model_tfidf = joblib.load('Models/lda_4_tfidf.joblib')
except FileNotFoundError:
    lda_model_tfidf = models.LdaMulticore(corpus_tfidf,
                                          num_topics=N_CLUSTERS,
                                          id2word=dictionary,
                                          passes=2,
                                          workers=4)
    joblib.dump(lda_model_tfidf, 'Models/lda_4_tfidf.joblib')


# In[42]:


for idx, topic in lda_model.print_topics(-1):
    print(f'Topic: {idx}')
    print(f'Words: {topic}')


# # Twitter Network<a id='part6'></a>
# 
# We create the csv file for Gephi to use.

# In[ ]:


net = df.filter(['text_raw', 'user'])
net['mentions'] = net['text_raw'].str.findall(r'@([^\s]\w+)')

out = []
for i in tnrange(len(net)):
    if len(net['mentions'].iloc[i]) == 0:
        out.append((net['user'].iloc[i], net['user'].iloc[i]))
    else:
        for mention in net['mentions'].iloc[i]:
            out.append((net['user'].iloc[i], mention))
            
edges = pd.DataFrame.from_records(out, columns=['Vertex 1', 'Vertex 2'])
edges.tail()


# We have to remove some nodes to allow computation. Let's remove the users that have tweeted less than 3 times.

# In[ ]:


edges['same'] = edges['Vertex 1'] == edges['Vertex 2']


# In[ ]:


counter = edges['Vertex 1'].value_counts().to_dict()
edges['count'] = edges['Vertex 1'].apply(lambda x: counter[x])


# In[ ]:


reduced = edges.query('count>3')


# In[ ]:


reduced.head()


# In[ ]:


edges.shape[0], reduced.shape[0], reduced.shape[0]/edges.shape[0]


# In[ ]:


edges.query('count>3', inplace=True)
edges.drop(columns=['same', 'count'], inplace=True)


# In[ ]:



edges.to_csv('edges.csv', index=False)

