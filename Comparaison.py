#!/usr/bin/env python
# coding: utf-8

# # Master Project Quick
# 
# This notebook contains the totality of the code used for my Master Project for quick analysis.

# In[4]:


import re
import json
import random

import joblib

import numpy as np
import pandas as pd

from PIL import Image

from tqdm import tnrange

from langdetect import detect
from langdetect.lang_detect_exception import LangDetectException

from wordcloud import WordCloud

import matplotlib.pyplot as plt

from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

from nltk.corpus import stopwords
from nltk.probability import FreqDist
from nltk.tokenize import regexp_tokenize
from nltk.stem import SnowballStemmer, WordNetLemmatizer
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk.collocations import BigramAssocMeasures, BigramCollocationFinder
from nltk.collocations import TrigramAssocMeasures, TrigramCollocationFinder

try:
    labels = ['fullname', 'html', 'id', 'likes', 'replies', 'retweets', 'text', 'timestamp', 'url', 'user']
    arr = np.load('Data/data.npy', allow_pickle=True)
    df_full = pd.DataFrame(arr, columns=labels)
except FileNotFoundError:
    with open('Data/01_09_2015-15_10_2015.json', 'r') as file:
        raw = json.load(file)
    df_full = pd.DataFrame(raw)
    np.save('Data/data.npy', df_full.values)
    df = df_full.drop(columns=['fullname', 'html', 'likes', 'replies', 'retweets', 'timestamp', 'url', 'user'])
df.head()
# In[5]:


class Processor():
    
    def __init__(self, df, df_full, first_batch=False):
        self.df = df
        self.df_full = df_full
        self.first_batch = first_batch
        
    def call(self):
        self.cleaner()
        
        if self.first_batch:
            splits = np.split(self.df, 27)
            clean_splits = []
            labels = ['id', 'text']

            for i in tnrange(len(splits)):
                try:
                    arr = np.load(f'Processing/split_{i}.npy', allow_pickle=True)
                    clean_splits.append(pd.DataFrame(arr, columns=['id', 'text']))
                    print(f'Loaded from index {splits[i].index._start} to {splits[i].index._stop}')
                except FileNotFoundError:
                    print(f'Cleaning from index {splits[i].index._start} to {splits[i].index._stop}')
                    df_clean = cleaner(splits[i])
                    df_english = english_keeper(df_clean)
                    clean_splits.append(df_english)
                    np.save(f'Processing/split_{i}.npy', df_english.values)
                    
            self.df = pd.concat(clean_splits)
            print(f'New shape {self.df.shape}')
            
        else:
            try:
                print(f'Original shape {self.df.shape}')
                arr = np.load(f'Processing/split2.npy', allow_pickle=True)
                self.df = pd.DataFrame(arr, columns=['id', 'text'])
                print(f'New shape {self.df.shape}')
            except FileNotFoundError:
                self.english_keeper()
                print(f'New shape {self.df.shape}')
                np.save(f'Processing/split2.npy', self.df.values)
            
        self.remove_stopwords(add=['volkswagen', 'vw'])
        
        self.lemmatizing()
        
        self.stemming()
        
        self.merge()
        
    def cleaner(self):
        print('Cleaning ...')
        
        # Links cleaning
        self.df['text'] = self.df['text'].str.replace(r'(https?:\/\/|pic.twitter)[^\s]+', '')

        # @ and # cleaning
        self.df['text'] = self.df['text'].str.replace(r'(#|@[^\s]\w+)', '')

        # Remove the numbers
        self.df['text'] = self.df['text'].str.replace(r'\d+', '')

        # Put everything to lower case
        self.df['text'] = self.df['text'].str.lower()

        # Punctuation
        self.df['text'] = self.df['text'].str.replace(r'[^\w\s]', ' ')

        # Strip whitespaces
        self.df['text'] = self.df['text'].str.replace(r'\s{2,}', ' ')

    def english_keeper(self):
        print('Removing non english words ...')
        
        english_tweets = []

        for i in tnrange(len(self.df)):
            text = self.df['text'].iloc[i]
            try:
                if detect(text) == 'en':
                    english_tweets.append((self.df['id'].iloc[i], text))
            except LangDetectException:
                pass

        self.df = pd.DataFrame.from_records(english_tweets, columns=['id', 'text'])
        
    def remove_stopwords(self, add=None):
        print('Removing stopwords ...')
        
        english_stopwords = stopwords.words('english')

        if type(add) == list:
            for e in add:
                english_stopwords.append(e)

        words_tweets = []

        for i in tnrange(len(self.df)):
            text = ' '.join([w for w in self.df['text'].iloc[i].split(' ') if w not in english_stopwords]).strip()
            words_tweets.append((self.df['id'].iloc[i], text))

        self.df = pd.DataFrame.from_records(words_tweets, columns=['id', 'text'])

        
    def lemmatizing(self):
        print('Lemmatizing ...')
        
        wnl = WordNetLemmatizer()

        lemmed_tweets = []

        for i in tnrange(len(self.df)):
            text = ' '.join([wnl.lemmatize(w, pos='v') for w in self.df['text'].iloc[i].split(' ')]).strip()
            lemmed_tweets.append((self.df['id'].iloc[i], text))

        self.df = pd.DataFrame.from_records(lemmed_tweets, columns=['id', 'text'])

        
    def stemming(self):
        print('Stemming ...')
        
        sno = SnowballStemmer('english')

        stemmed_tweets = []

        for i in tnrange(len(self.df)):
            text = ' '.join([sno.stem(w) for w in self.df['text'].iloc[i].split(' ')]).strip()
            stemmed_tweets.append((self.df['id'].iloc[i], text))

        self.df = pd.DataFrame.from_records(stemmed_tweets, columns=['id', 'text'])

        
    def merge(self):
        self.df = pd.merge(self.df, self.df_full, how='left', on='id', suffixes=('_work', '_raw'))
        self.df['timestamp'] = pd.to_datetime(self.df['timestamp'])

        print(f'The shape is now {self.df.shape}')

process_1 = Processor(df, df_full, first_batch=True)
process_1.call()
# In[6]:


try:
    labels = ['fullname', 'html', 'id', 'likes', 'replies', 'retweets', 'text', 'timestamp', 'url', 'user']
    arr = np.load('Data/data2.npy', allow_pickle=True)
    df_full2 = pd.DataFrame(arr, columns=labels)
except FileNotFoundError:
    with open('Data/01_09_2016-15_10_2016.json', 'r') as file:
        raw = json.load(file)
    df_full2 = pd.DataFrame(raw)
    np.save('Data/data2.npy', df_full2.values)


# In[7]:


df2 = df_full2.drop(columns=['fullname', 'html', 'likes', 'replies', 'retweets', 'timestamp', 'url', 'user'])
df2.head()


# In[8]:


process_2 = Processor(df2, df_full2, first_batch=False)
process_2.call()


# In[9]:


df2 = process_2.df


# In[10]:


sid = SentimentIntensityAnalyzer()
print(df2['text_work'].iloc[0])
print(sid.polarity_scores(df2['text_work'].iloc[0]))


# In[11]:


df2['compound'] = df2['text_raw'].apply(lambda x: sid.polarity_scores(x)['compound'])


# In[12]:


df2.head()

df2.to_csv('Data/tableau2.csv', index=False)
# In[23]:


df2.query('timestamp>"2016-10-01 00:00:00" and timestamp<"2016-10-01 23:59:59"')['text_raw']


# In[24]:


df2.query('timestamp>"2016-10-01 00:00:00" and timestamp<"2016-10-01 23:59:59"')['text_raw'][75253]


# In[ ]:




